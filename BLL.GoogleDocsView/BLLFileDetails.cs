﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace BLL.GoogleDocsView
{
    public class BLLFileDetails
    {
        public DataSet GetALLFoldersByClient(int clientId)
        {
            try
            {
                SqlParameter[] sqlParams = { new SqlParameter("@ClientId", clientId)};
                return SqlHelper.ExecuteDataset(CommonMethods.ConnectionString, "USP_GetALLFoldersByClient", sqlParams);
            }
            catch(Exception ex)
            {
                throw new Exceptions.DatabaseOperationException("Unable to get Folder details due to a database error.", ex);
            }
        }

        public DataSet GetAllFolderIdsByUserId(int userId)
        {
            try
            {
                SqlParameter[] sqlParams = { new SqlParameter("@UserId", userId) };
                return SqlHelper.ExecuteDataset(CommonMethods.ConnectionString, "USP_GetALLFoldersIdsByUserId", sqlParams);
            }
            catch (Exception ex)
            {
                throw new Exceptions.DatabaseOperationException("Unable to get Folder Ids due to a database error.", ex);
            }
        }

        public DataSet GetAllFolderNameByFolderId(string folderIds)
        {
            try
            {
                SqlParameter[] sqlParams = { new SqlParameter("@folderIds", folderIds) };
                return SqlHelper.ExecuteDataset(CommonMethods.ConnectionString, "USP_GetAllFolderNameByFolderId", sqlParams);
            }
            catch (Exception ex)
            {
                throw new Exceptions.DatabaseOperationException("Unable to get Folder Ids due to a database error.", ex);
            }
        }
    }
}
