﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;


namespace BLL.GoogleDocsView
{
    public class UserDetails
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int RoleId { get; set; }
        public int ClientId { get; set; }
        public string FolderIds { get; set; }
        public string RoleName { get; set; }
        public string ClientName { get; set; }
    }
    public class BLLLogin 
    {
        public bool CheckUsernamePasswordForEmpty(string userName, string password)
        {
            if (userName != "" && password != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public UserDetails ValidateUser(string userName, string password)
        {
            try
            {
                DataSet ds =  SqlHelper.ExecuteDataset(CommonMethods.ConnectionString, "Validate_User", userName, password);

                UserDetails userDetails = new UserDetails();

                userDetails.UserId      = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                userDetails.ClientId    = Convert.ToInt32(ds.Tables[0].Rows[0]["ClientsId"].ToString() == "" ? "0": ds.Tables[0].Rows[0]["ClientsId"]);
                userDetails.RoleName    = ds.Tables[0].Rows[0]["Roles"].ToString();
                userDetails.ClientName  = ds.Tables[0].Rows[0]["Clients"].ToString();

                return userDetails;
            }
            catch (Exception ex)
            {
                throw new Exceptions.DatabaseOperationException("Unable to get user details due to a database error.", ex);
            }
        }
    }
}
