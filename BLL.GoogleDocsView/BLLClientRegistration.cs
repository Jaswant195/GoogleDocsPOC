﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace BLL.GoogleDocsView
{
    public class BLLClientRegistration
    {
        public int UpsertClientDetails(string ClientID,string ClientName,string Action,string CheckDuplicate)
        {
            try
            {
                SqlParameter[] sqlParams = { new SqlParameter("@ClientID", ClientID),
                                             new SqlParameter("@ClientName", ClientName),
                                             new SqlParameter("@Action", Action),
                                             new SqlParameter("@CheckDuplicate", CheckDuplicate)};

                return Convert.ToInt32(SqlHelper.ExecuteScalar(CommonMethods.ConnectionString, "AddUpdateClient", sqlParams));
            }
            catch (Exception ex)
            {
                throw new Exceptions.DatabaseOperationException("Unable to insert client details due to a database error.", ex);
            }
        }

        public DataSet BindClientDetails()
        {
            try
            {
                return SqlHelper.ExecuteDataset(CommonMethods.ConnectionString, "USP_GetAllClientDetails", null);
            }
            catch (Exception ex)
            {
                throw new Exceptions.DatabaseOperationException("Unable to Fetch client details due to a database error.", ex);
            }
        }

        public int DeleteClient(string clientId)
        {
            try
            {
                SqlParameter[] sqlParams = { new SqlParameter("@ClientId", clientId) };
                return SqlHelper.ExecuteNonQuery(CommonMethods.ConnectionString, "USP_DeleteClient", sqlParams);
            }
            catch (Exception ex)
            {
                throw new Exceptions.DatabaseOperationException("Unable to delete client details due to a database error.", ex);
            }
        }
    }
}
