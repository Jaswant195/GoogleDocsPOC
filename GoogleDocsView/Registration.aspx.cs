﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoogleDocsView
{
    public partial class Registration : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
        public string _roleName = string.Empty;
        public string _clientName = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            txtPassword.Attributes["type"] = "password";
            txtConfirmPassword.Attributes["type"] = "password";
            
            _roleName = Session["RoleName"].ToString();
            _clientName = Session["ClientName"].ToString();
            if (!this.IsPostBack)
            {
                //_roleName = Session["RoleName"].ToString();
                //_clientName = Session["ClientName"].ToString();
                BindData();
                
                
            }
        }

        private void BindData()
        {
            string userName = this.User.Identity.Name.ToString();
            SqlConnection con = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter sda = new SqlDataAdapter();
            DataTable dt = new DataTable();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetEmployeeDetailsByClient";
            cmd.Parameters.Add("@RoleName", SqlDbType.VarChar, 20).Value = _roleName;
            cmd.Parameters.Add("@ClientName", SqlDbType.VarChar,100).Value = _clientName;
            cmd.Parameters.Add("@UserName", SqlDbType.VarChar, 100).Value = userName;
            cmd.Connection = con;
            try
            {
                con.Open();
                clientGrid.EmptyDataText = "No Records Found";
                sda.SelectCommand = cmd;
                sda.Fill(dt);
                clientGrid.DataSource = dt;
                clientGrid.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        private DataTable GetData(SqlCommand cmd)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    con.Open();
                    sda.SelectCommand = cmd;
                    sda.Fill(dt);
                    return dt;
                }
            }
        }

        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            this.BindData();
            clientGrid.PageIndex = e.NewPageIndex;
            clientGrid.DataBind();
        }

        protected void Edit(object sender, EventArgs e)
        {
            using (GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent)
            {
                ddlClients.Items.Clear();
                FillClientDropdown();
                ddlFolders.Items.Clear();
                
                txtUserId.Text = row.Cells[0].Text;
                txtUsername.Text = row.Cells[1].Text;
                txtPassword.Text = row.Cells[2].Text;
                txtConfirmPassword.Text = row.Cells[2].Text;
                txtEmail.Text = row.Cells[3].Text;
                ddlRole.SelectedIndex = ddlRole.Items.IndexOf(ddlRole.Items.FindByText(row.Cells[4].Text));
                ddlClients.SelectedIndex = ddlClients.Items.IndexOf(ddlClients.Items.FindByText(row.Cells[5].Text));
                FillFoldersDropdown(Convert.ToInt32(ddlClients.SelectedValue.ToString()));
                string folderIds = row.Cells[6].Text;
                if (folderIds!="NO")
                {
                    List<string> stringList = folderIds.Split(',').ToList();
                    foreach (string str in stringList)
                    {
                        ddlFolders.Items.FindByValue(str).Selected = true;
                    }
                }
                
                ScriptManager.RegisterStartupScript(this, typeof(Page), "myFunction", "myFunction();", true);
                if (_roleName == "Administrator")
                {
                    ddlRole.Enabled = false;
                    ddlClients.Enabled = false;
                }
                if (_roleName == "SuperAdministrator")
                {
                    ddlRole.Enabled = true;
                    ddlClients.Enabled = true;
                }

                pnlHeading.InnerText = "Update User";
                btnSave.Text = "Update";
                popup.Show();
            }
        }

        protected void Add(object sender, EventArgs e)
        {

            txtUserId.Text = string.Empty;
            txtUsername.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtConfirmPassword.Text = string.Empty;
            txtEmail.Text = string.Empty;
            ddlClients.Items.Clear();
            FillClientDropdown();
            if (_roleName == "Administrator")
            {
                ddlRole.SelectedIndex =ddlRole.Items.IndexOf(ddlRole.Items.FindByText("User"));
                ddlRole.Enabled = false;
                ddlClients.SelectedIndex = ddlClients.Items.IndexOf(ddlClients.Items.FindByText(_clientName));
                ddlClients.Enabled = false;
                ddlFolders.Items.Clear();
                FillFoldersDropdown(Convert.ToInt32(ddlClients.SelectedValue.ToString()));
                ScriptManager.RegisterStartupScript(this, typeof(Page), "myFunction", "myFunction();", true);
            }
            if (_roleName== "SuperAdministrator")
            {
                ddlRole.SelectedIndex = ddlRole.Items.IndexOf(ddlRole.Items.FindByText("--Select--"));
                ddlRole.Enabled = true;
                ddlClients.Enabled = true;

            }
            pnlHeading.InnerText = "Add User";
            btnSave.Text = "Add";
            popup.Show();
        }

        protected void Save(object sender, EventArgs e)
        {
            
                int userId = 0;
                string folderIds = FolderIds();
                SqlConnection con = new SqlConnection(constr);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "AddUpdateUser";
                cmd.Parameters.AddWithValue("@UserId", txtUserId.Text.Trim());
                cmd.Parameters.AddWithValue("@Username", txtUsername.Text.Trim());
                cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Role", Convert.ToInt32(ddlRole.SelectedValue.ToString()));
                cmd.Parameters.AddWithValue("@Client", Convert.ToInt32(ddlClients.SelectedValue.ToString()));
                cmd.Parameters.AddWithValue("@Action", btnSave.Text.Trim());
                cmd.Parameters.AddWithValue("@FolderIds", folderIds);
                cmd.Connection = con;
                con.Open();
                userId = Convert.ToInt32(cmd.ExecuteScalar());
                con.Close();
                string message = string.Empty;
                switch (userId)
                {
                    case -1:
                    message = "Username already exists.\\nPlease choose a different username.";
                    popup.Show();
                    break;
                    case -2:
                        message = "Supplied email address has already been used.";
                        break;
                    case -3:
                        message = "updated Successfully";
                        BindData();
                        break;
                    default:
                        message = "Registration successful. Activation email has been sent.";
                    //SendActivationEmail(userId);
                    BindData();
                        break;
                }
           
            
            ScriptManager.RegisterStartupScript(this, typeof(Page), "Alert", "alert('" + message + "');", true);
              
            
        }

        protected void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ddlFolders.Items.Clear();
            FillFoldersDropdown(Convert.ToInt32(ddlClients.SelectedValue.ToString()));
            ScriptManager.RegisterStartupScript(this, typeof(Page), "myFunction", "myFunction();", true);
            popup.Show();
        }

        private string FolderIds()
        {
            string folderIds = "";
            foreach (ListItem item in ddlFolders.Items)
            {
                if (item.Selected)
                {
                    folderIds += item.Value + ",";
                }
            }
            folderIds = folderIds.Remove(folderIds.Length - 1);
            return folderIds;
        }
        protected void DeleteUser(object sender, EventArgs e)
        {
            LinkButton lnkRemove = (LinkButton)sender;
            SqlConnection con = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "DELETE FROM Users WHERE " +
            " UserId=@UserId";
            cmd.Parameters.Add("@UserId", SqlDbType.VarChar).Value
                = lnkRemove.CommandArgument;
            cmd.Connection = con;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            BindData();
        }

        protected void FillClientDropdown()
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT Id, Name FROM Clients"))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    ddlClients.DataSource = cmd.ExecuteReader();
                    ddlClients.DataTextField = "Name";
                    ddlClients.DataValueField = "Id";
                    ddlClients.DataBind();
                    con.Close();
                }
            }
            ddlClients.Items.Insert(0, new ListItem("--Select--", "0"));
        }


        protected void FillFoldersDropdown(int ClientId)
        {
            //string executeQuery;

            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT ID, FolderName FROM Folders WHERE ClientId="+ClientId+""))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    ddlFolders.DataSource = cmd.ExecuteReader();
                    ddlFolders.DataTextField = "FolderName";
                    ddlFolders.DataValueField = "ID";
                    ddlFolders.DataBind();
                    con.Close();
                }
            }
            
        }

        private void SendActivationEmail(int userId)
        {
            string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            string activationCode = Guid.NewGuid().ToString();
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("INSERT INTO UserActivation VALUES(@UserId, @ActivationCode)"))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@UserId", userId);
                        cmd.Parameters.AddWithValue("@ActivationCode", activationCode);
                        cmd.Connection = con;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
            using (MailMessage mm = new MailMessage("ramkrishnareddy798@gmail.com", txtEmail.Text))
            {
                mm.Subject = "Account Activation";
                string body = "Hello " + txtUsername.Text.Trim() + ",";
                body += "<br /><br />Please click the following link to activate your account";
                body += "<br /><a href = '" + Request.Url.AbsoluteUri.Replace("Registration.aspx", "UserActivation.aspx?ActivationCode=" + activationCode) + "'>Click here to activate your account.</a>";
                body += "<br /><br />Thanks";
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential("ramkrishnareddy798@gmail.com", "Rama@1617");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }
        }
    }
}