﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileDetails.aspx.cs" EnableEventValidation="false" MasterPageFile="~/GoogleDocs.Master" Inherits="GoogleDocsView.FileDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <head>
        <title>File Details</title>
        <link href="CSS/CSS.css" rel="stylesheet" type="text/css" />
        <style>
            #TreeView1 table tbody tr td a:hover {
                text-decoration: underline;
                color: red;
            }
        </style>
        <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#TreeView1 table tbody tr td a").click(function () {
                this.href = "";
                return true;
            });
        });
    </script>--%>
    </head>
    <%--oncontextmenu="return false;"--%>
    <body>
        <form id="form1">
            <div style="margin-top: 15px;">
                <asp:HyperLink ID="hplClient" CssClass="btn" Visible="false" NavigateUrl="~/ClientRegistration.aspx" Style="margin-left: 5px; margin-top: 10px;" runat="server" Text="Client"></asp:HyperLink>
                <asp:HyperLink ID="hplRegistration" Visible="false" CssClass="btn" NavigateUrl="~/Registration.aspx" Style="margin-left: 5px;" runat="server" Text="Registration"></asp:HyperLink>
                <asp:HyperLink ID="hplFolder" Visible="false" CssClass="btn" NavigateUrl="~/Folder.aspx" Style="margin-left: 5px;" runat="server" Text="Folders"></asp:HyperLink>
            </div>
            <div style="width: 100%; height: 100%; overflow: hidden;">
                <h3>File Details</h3>

                <div style="float: left; margin-left: 10px; width: 35%;">
                    <asp:TreeView ID="TreeView1" runat="server" ImageSet="XPFileExplorer" OnSelectedNodeChanged="TreeView1_SelectedNodeChanged" NodeIndent="15" Target="ContentRightFrame">
                        <HoverNodeStyle Font-Underline="True" ForeColor="#6666AA" />
                        <NodeStyle Font-Names="Tahoma" Font-Size="8pt" ForeColor="Black" HorizontalPadding="2px"
                            NodeSpacing="0px" VerticalPadding="2px"></NodeStyle>
                        <ParentNodeStyle Font-Bold="False" />
                        <SelectedNodeStyle BackColor="#B5B5B5" Font-Underline="False" HorizontalPadding="0px"
                            VerticalPadding="0px" />
                    </asp:TreeView>
                    <asp:Label ID="lblMessage" runat="server" Text="" Visible="false"></asp:Label>
                </div>
                <div style="float: right; margin-left: 10px; width: 63%;">
                    <iframe id="ContentRightFrame" runat="server" name="ContentRightFrame" style="position: relative; top: -116px;" width="100%" height="650" frameborder="1"></iframe>

                </div>
            </div>
        </form>
</asp:Content>
