﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" EnableEventValidation="false" MasterPageFile="~/GoogleDocs.Master" Inherits="GoogleDocsView.Registration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<head id="Head1">
    <title>Registration Page</title>
    <link href="CSS/CSS.css" rel="stylesheet" type="text/css" /> 
     <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <%--<script src="scripts/jquery-1.3.2.min.js" type="text/javascript"></script>--%>
<script src="scripts/jquery.blockUI.js" type="text/javascript"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css"
    rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
<link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
<script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js" type="text/javascript"></script>

    <script type = "text/javascript">
    function BlockUI(elementID) {
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(function() {
            $("#" + elementID).block({ message: '<table align = "center"><tr><td>' +
     '<img src="images/loadingAnim.gif"/></td></tr></table>',
                css: {},
                overlayCSS: { backgroundColor: '#000000', opacity: 0.6
                }
            });
        });
        prm.add_endRequest(function() {
            $("#" + elementID).unblock();
        });
    }
    $(document).ready(function() {
 
        BlockUI("<%=pnlAddEdit.ClientID %>");
        $.blockUI.defaults.css = {};
    });
    function Hidepopup() {
        $find("popup").hide();
        return false;
    }
    function myFunction() {
        $(function () {
            $('[id*=ddlFolders]').multiselect({
                includeSelectAllOption: true
            });
        });
    }
</script>
    <style type="text/css">
    </style>
</head>
<body style ="margin:0;padding:0">
    <form id="form1" >
<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
    <div style="margin-top:100px;">
    <asp:Button ID="btnAdd" CssClass="btn" style="position:absolute; margin-left:-110px;left:50%;top:5px;cursor:pointer;width:80px;margin-top:50px;" runat="server" Text="Add" OnClick = "Add" />
        <asp:HyperLink ID="hplBack" NavigateUrl="~/FileDetails.aspx" CssClass="btn" style="position:absolute; left:50%;top:5px;cursor:pointer;margin-top:50px;" runat="server" Text="BACK" />
    </div>
<asp:GridView ID="clientGrid" runat="server" horizontalalign="Center" style="margin-top:55px;" 
AutoGenerateColumns = "false"  AlternatingRowStyle-BackColor = "#C2D69B"  
HeaderStyle-BackColor = "green" AllowPaging ="true" HeaderStyle-ForeColor="White"
OnPageIndexChanging = "OnPaging" 
PageSize = "10" >
<Columns>
<asp:BoundField DataField = "UserId" HeaderText = "User ID" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HtmlEncode = "true" />
<asp:BoundField DataField = "Username" HeaderText = "User Name"  HtmlEncode = "true" />
<asp:BoundField DataField = "Password" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText = "Password" HtmlEncode = "true" />
<asp:BoundField DataField = "Email" HeaderText = "Email"  HtmlEncode = "true" />
<asp:BoundField DataField = "RoleName" HeaderText = "Role Name" HtmlEncode = "true" />
<asp:BoundField DataField = "Name" HeaderText = "Client Name"  HtmlEncode = "true" />
<asp:BoundField DataField = "FolderIds" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText = "Folder Ids"  HtmlEncode = "true" />
<asp:TemplateField ItemStyle-Width = "40px"  HeaderText = "Edit">
   <ItemTemplate>
       <asp:LinkButton ID="lnkEdit" runat="server" Text = "Edit" OnClick = "Edit"></asp:LinkButton>
   </ItemTemplate>
</asp:TemplateField>
    <asp:TemplateField ItemStyle-Width = "40px"  HeaderText = "Delete">
    <ItemTemplate>
        <asp:LinkButton ID="lnkRemove" runat="server"
            CommandArgument = '<%# Eval("UserId")%>'
         OnClientClick = "return confirm('Do you want to delete?')"
        Text = "Delete" OnClick = "DeleteUser"></asp:LinkButton>
    </ItemTemplate>
</asp:TemplateField>
</Columns> 
</asp:GridView>


<asp:Panel ID="pnlAddEdit" runat="server" CssClass="modalPopup" style = "display:none;width:600px;height:400px;">
<div runat="server" id="pnlHeading" style="text-align:center;font-weight:bold;">User Details</div>
<br />
         <table id="PnlTable" align = "center" style="width:520px;">
             <tr runat="server" style="visibility:hidden;">
                <td>UserId
                </td>
                <td>
                    <asp:TextBox ID="txtUserId" runat="server"/>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>Username
                </td>
                <td>
                    <asp:TextBox ID="txtUsername" runat="server" ValidationGroup="Popup"/>
                </td>
                <td>
                     <asp:RequiredFieldValidator  ID="RequiredFieldValidator1" runat="server" ForeColor="Red" ControlToValidate="txtUsername" ErrorMessage="Username Required"
                           Display="Dynamic" SetFocusOnError="true" ValidationGroup="Popup"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Password
                </td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" ValidationGroup="Popup"/>
                </td>
                <td>
                     <asp:RequiredFieldValidator  ID="RequiredFieldValidator2" runat="server" ForeColor="Red" ControlToValidate="txtPassword" ErrorMessage="Password Required"
                           Display="Dynamic" SetFocusOnError="true" ValidationGroup="Popup"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Confirm Password
                </td>
                <td>
                    <asp:TextBox ID="txtConfirmPassword" runat="server" ValidationGroup="Popup" />
                </td>
                <td>
                    <asp:RequiredFieldValidator  ID="RequiredFieldValidator3" runat="server" ForeColor="Red" ControlToCompare="txtPassword"
                        ControlToValidate="txtConfirmPassword" ErrorMessage="Passwords do not match." Display="Dynamic" SetFocusOnError="true" ValidationGroup="Popup"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Email
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" ValidationGroup="Popup" />
                </td>
                <td>
                    <asp:RequiredFieldValidator  ID="RequiredFieldValidator4" runat="server" ForeColor="Red" ControlToValidate="txtEmail" 
                        ErrorMessage="Email is Required." Display="Dynamic" SetFocusOnError="true" ValidationGroup="Popup"></asp:RequiredFieldValidator>

                    <asp:RegularExpressionValidator runat="server" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ControlToValidate="txtEmail" ForeColor="Red" ErrorMessage="Invalid email address." SetFocusOnError="true" ValidationGroup="Popup"/>
                </td>
            </tr>
            <tr>
                <td>Role
                </td>
                <td>
                    <asp:DropDownList ID="ddlRole" runat="server" AppendDataBoundItems="True" ValidationGroup="Popup">
                        <asp:ListItem>--Select--</asp:ListItem>
                        <asp:ListItem Value="1">Administrator</asp:ListItem>
                        <asp:ListItem Value="2">User</asp:ListItem>
                        <asp:ListItem Value="3">SuperAdministrator</asp:ListItem>
                    </asp:DropDownList> 
                </td>
                <td>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlRole" ForeColor="Red" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Popup"
                         ErrorMessage="Value Required!" InitialValue="--Select--"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Client
                </td>
                <td>
                    <asp:DropDownList ID="ddlClients" runat="server" AutoPostBack = "true" OnSelectedIndexChanged = "OnSelectedIndexChanged" AppendDataBoundItems="True" ValidationGroup="Popup">
                        
                    </asp:DropDownList>
                </td>
                <td>
             
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlClients" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Popup"
                        ErrorMessage="Value Required!" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                </td>
            </tr>
             <tr>
                <td>Folders
                </td>
                <td>
                    <asp:ListBox ID="ddlFolders" runat="server" AppendDataBoundItems="True" ValidationGroup="Popup" SelectionMode="Multiple">
                        
                    </asp:ListBox>
                </td>
                <td>
             
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlFolders" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Popup"
                        ErrorMessage="Value Required!" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
<div style="text-align:center;top:10px;">
<asp:Button ID="btnSave" runat="server" CssClass="btn" Text="" OnClick = "Save" ValidationGroup="Popup" style="cursor:pointer;width:100px;"/>
<asp:Button ID="btnCancel" runat="server" CssClass="btn" Text="Cancel" style="cursor:pointer;width:100px;" OnClientClick = "return Hidepopup()"/>
</div>
</asp:Panel>
<asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>
<cc1:ModalPopupExtender ID="popup" runat="server" DropShadow="false"
PopupControlID="pnlAddEdit" TargetControlID = "lnkFake"
BackgroundCssClass="modalBackground">
</cc1:ModalPopupExtender>
    
</ContentTemplate> 
<Triggers>
<asp:AsyncPostBackTrigger ControlID = "clientGrid" />
<asp:AsyncPostBackTrigger ControlID = "btnSave" />
</Triggers> 
</asp:UpdatePanel> 
    </form>
</body>
</asp:content>
