﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClientRegistration.aspx.cs" EnableEventValidation="false" MasterPageFile="~/GoogleDocs.Master" Inherits="GoogleDocsView.ClientRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


    <head id="Head1">
        <title>Client Registration Page</title>
        <link href="CSS/CSS.css" rel="stylesheet" type="text/css" />
        <script src="scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
        <script src="scripts/jquery.blockUI.js" type="text/javascript"></script>
        <script type="text/javascript">
            function BlockUI(elementID) {
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_beginRequest(function () {
                    $("#" + elementID).block({
                        message: '<table align = "center"><tr><td>' +
                 '<img src="images/loadingAnim.gif"/></td></tr></table>',
                        css: {},
                        overlayCSS: {
                            backgroundColor: '#000000', opacity: 0.6
                        }
                    });
                });
                prm.add_endRequest(function () {
                    $("#" + elementID).unblock();
                });
            }
            $(document).ready(function () {

                BlockUI("<%=pnlAddEdit.ClientID %>");
        $.blockUI.defaults.css = {};
    });
        function Hidepopup() {
            $find("popup").hide();
            return false;
        }
        </script>
        <style>
 
    </style>
    </head>
    <body style="margin: 0; padding: 0">
        <form id="form1">
            <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div style="margin-top: 100px;">
                        <asp:Button ID="btnAdd" CssClass="btn" Style="position: absolute; margin-left: -110px; left: 50%; top: 5px; cursor: pointer; width: 80px; margin-top: 50px;" runat="server" Text="Add" OnClick="AddClient" />
                        <asp:HyperLink ID="hplBack" NavigateUrl="~/FileDetails.aspx" CssClass="btn" Style="position: absolute; left: 50%; top: 5px; cursor: pointer; margin-top: 50px;" runat="server" Text="BACK" />
                    </div>
                    <asp:GridView ID="clientGrid" runat="server" HorizontalAlign="Center" Style="margin-top: 55px;" Width="550px"
                        AutoGenerateColumns="false" AlternatingRowStyle-BackColor="#C2D69B"
                        HeaderStyle-BackColor="green" AllowPaging="true" HeaderStyle-ForeColor="White"
                        OnPageIndexChanging="OnPaging"
                        PageSize="10">
                        <Columns>
                            <asp:BoundField DataField="Id" HeaderText="Client ID" HtmlEncode="true" />
                            <asp:BoundField DataField="Name" HeaderText="Client Name" HtmlEncode="true" />
                            <asp:TemplateField ItemStyle-Width="40px" HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" OnClick="Edit"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="40px" HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkRemove" runat="server"
                                        CommandArgument='<%# Eval("Id")%>'
                                        OnClientClick="return confirm('Do you want to delete?')"
                                        Text="Delete" OnClick="DeleteClient"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:HiddenField ID="hdnClientName" runat="server" />

                    <asp:Panel ID="pnlAddEdit" runat="server" CssClass="modalPopup" Style="display: none; width: 600px">
                        <div runat="server" id="pnlHeading" style="text-align: center; font-weight: bold;">Client Details</div>
                        <br />
                        <table id="PnlTable" align="center" style="width: 520px;">
                            <tr runat="server" id="tblCustomerIdRow">
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text="ClientId :"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtClientID" Width="40px" MaxLength="5" runat="server"></asp:TextBox>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Client Name :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtClientName" runat="server" ValidationGroup="Popup"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="Red" ControlToValidate="txtClientName" ErrorMessage="ClientName Required"
                                        Display="Dynamic" SetFocusOnError="true" ValidationGroup="Popup"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                        <div style="text-align: center; top: 10px;">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn" Text="" OnClick="SaveClient" Style="cursor: pointer; width: 80px;" ValidationGroup="Popup" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="btn" Text="Cancel" Style="cursor: pointer; width: 80px;" OnClientClick="return Hidepopup()" />
                        </div>
                    </asp:Panel>
                    <asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>
                    <cc1:ModalPopupExtender ID="popup" runat="server" DropShadow="false"
                        PopupControlID="pnlAddEdit" TargetControlID="lnkFake"
                        BackgroundCssClass="modalBackground">
                    </cc1:ModalPopupExtender>

                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="clientGrid" />
                    <asp:AsyncPostBackTrigger ControlID="btnSave" />
                </Triggers>
            </asp:UpdatePanel>
        </form>
    </body>
</asp:Content>

