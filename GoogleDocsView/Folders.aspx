﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Folders.aspx.cs" EnableEventValidation="false" MasterPageFile="~/GoogleDocs.Master" Inherits="GoogleDocsView.Folders" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<head id="Head1">
    <title>Folders Page</title>
    <link href="CSS/CSS.css" rel="stylesheet" type="text/css" /> 
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <%--<script src="scripts/jquery-1.3.2.min.js" type="text/javascript"></script>--%>
<script src="scripts/jquery.blockUI.js" type="text/javascript"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css"
    rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
<link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
<script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js" type="text/javascript"></script>
<script type = "text/javascript">
    function BlockUI(elementID) {
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(function() {
            $("#" + elementID).block({ message: '<table align = "center"><tr><td>' +
     '<img src="images/loadingAnim.gif"/></td></tr></table>',
                css: {},
                overlayCSS: { backgroundColor: '#000000', opacity: 0.6
                }
            });
        });
        prm.add_endRequest(function() {
            $("#" + elementID).unblock();
        });
    }
    $(document).ready(function() {
 
        BlockUI("<%=pnlAddEdit.ClientID %>");
        $.blockUI.defaults.css = {};
       
    });
    function Hidepopup() {
        $find("popup").hide();
        return false;
    }
    function myFunction() {
        $(function () {
            $('[id*=ddlClients]').multiselect({
                includeSelectAllOption: true
            });
        });
      }
</script>
    <style>
   
    </style>
</head>
<body style ="margin:0;padding:0">
    <form id="form1">
<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
    <div style="margin-top:100px;">
    <asp:Button ID="btnAdd" CssClass="btn" style="position:absolute; margin-left:-110px;left:50%;top:5px;cursor:pointer;width:80px;margin-top:50px;" runat="server" Text="Add" OnClick = "Add" />
        <asp:HyperLink ID="hplBack" NavigateUrl="~/FileDetails.aspx" CssClass="btn" style="position:absolute; left:50%;top:5px;cursor:pointer;margin-top:50px;" runat="server" Text="BACK" />
    </div>
<asp:GridView ID="foldersGrid" runat="server" horizontalalign="Center" style="margin-top:55px;" Width = "550px"
AutoGenerateColumns = "false"  AlternatingRowStyle-BackColor = "#C2D69B"  
HeaderStyle-BackColor = "green" AllowPaging ="true" HeaderStyle-ForeColor="White"
OnPageIndexChanging = "OnPaging" 
PageSize = "10" >
<Columns>
    <asp:BoundField DataField = "ID" HeaderText = "Folder ID" HtmlEncode = "true" />
    <asp:BoundField DataField = "FolderName" HeaderText = "Folder Name"  HtmlEncode = "true" />
    <asp:BoundField DataField = "ClientId" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText = "Client Name"  HtmlEncode = "true" />
<asp:TemplateField ItemStyle-Width = "40px"  HeaderText = "Edit">
   <ItemTemplate>
       <asp:LinkButton ID="lnkEdit" runat="server" Text = "Edit" OnClick = "Edit"></asp:LinkButton>
   </ItemTemplate>
</asp:TemplateField>
    <asp:TemplateField ItemStyle-Width = "40px"  HeaderText = "Delete">
    <ItemTemplate>
        <asp:LinkButton ID="lnkRemove" runat="server"
            CommandArgument = '<%# Eval("ID")%>'
         OnClientClick = "return confirm('Do you want to delete?')"
        Text = "Delete" OnClick = "DeleteFolder"></asp:LinkButton>
    </ItemTemplate>
</asp:TemplateField>
</Columns> 
</asp:GridView>
<asp:HiddenField ID="hdnFolderName" runat="server" />
<asp:HiddenField ID="hdnClientId" runat="server" />

<asp:Panel ID="pnlAddEdit" runat="server" CssClass="modalPopup" style = "display:none;width:600px;height:250px;">
<div runat="server" id="pnlHeading" style="text-align:center;font-weight:bold;">Client Details</div>
<br />
<table id="PnlTable" align = "center" style="width:520px;">
<tr runat="server" id="tblFolderIdRow">
<td>
Folder Id :"
</td>
<td>
<asp:TextBox ID="txtFolderId" Width = "40px" MaxLength = "5" runat="server"></asp:TextBox>
</td>
</tr>
<tr>
<td>
Folder Name :
</td>
<td>
<asp:TextBox ID="txtFolderName" runat="server" ValidationGroup="Popup"></asp:TextBox>    
</td>
<td>
<asp:RequiredFieldValidator  ID="RequiredFieldValidator1" runat="server" ForeColor="Red" ControlToValidate="txtFolderName" ErrorMessage="FolderName Required"
Display="Dynamic" SetFocusOnError="true" ValidationGroup="Popup"></asp:RequiredFieldValidator>
</td>
</tr>
<tr>
<td>
Client Name :
</td>
<td>
<asp:ListBox ID="ddlClients" runat="server" SelectionMode="Multiple"></asp:ListBox>
</td>
<td>           
<asp:RequiredFieldValidator runat="server" ControlToValidate="ddlClients" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Popup"
ErrorMessage="Value Required!" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
</td>
</tr>
</table>
<div style="text-align:center;top:10px;">
<asp:Button ID="btnSave" runat="server" CssClass="btn" Text="" OnClick = "Save" style="cursor:pointer;" ValidationGroup="Popup"/>
<asp:Button ID="btnCancel" runat="server" CssClass="btn" Text="Cancel" style="cursor:pointer;" OnClientClick = "return Hidepopup()"/>
</div>
</asp:Panel>
<asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>
<cc1:ModalPopupExtender ID="popup" runat="server" DropShadow="false"
PopupControlID="pnlAddEdit" TargetControlID = "lnkFake"
BackgroundCssClass="modalBackground">
</cc1:ModalPopupExtender>
    
</ContentTemplate> 
<Triggers>
<asp:AsyncPostBackTrigger ControlID = "foldersGrid" />
<asp:AsyncPostBackTrigger ControlID = "btnSave" />
</Triggers> 
</asp:UpdatePanel> 
    </form>
</body>
</asp:content>
