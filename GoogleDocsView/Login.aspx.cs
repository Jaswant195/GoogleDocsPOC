﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Security;
using BLL.GoogleDocsView;

namespace GoogleDocsView
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            int userId = 0;

            string userName = txtUserName.Text.Trim();
            string password = txtPassword.Text.Trim();

            if (new BLLLogin().CheckUsernamePasswordForEmpty(userName, password))
            {
                UserDetails userDetails = new UserDetails();

                userDetails = new BLLLogin().ValidateUser(userName, password);

                userId = userDetails.UserId; 

                switch (userId)
                {
                    case -1:
                        lblError.Text = "Username and/or password is incorrect.";
                        break;
                    case -2:
                        lblError.Text = "Account has not been activated.";
                        break;
                    default:
                        Session["UserId"] = userDetails.UserId;
                        Session["RoleName"] = userDetails.RoleName;
                        Session["ClientName"] = userDetails.ClientName;
                        Session["UserName"] = userName;
                        Session["ClientId"] = userDetails.ClientId;
                        FormsAuthentication.RedirectFromLoginPage(userName, true);
                        break;
                }
            }
            else
            {
                lblError.Text = "Username and Password can't be empty";
            }
        }
    }
}