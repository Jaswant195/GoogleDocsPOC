﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoogleDocsView
{
    public partial class Folders : System.Web.UI.Page
    {
        private String constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.BindData();
            }
        }

        private void BindData()
        {
            //string userName = this.User.Identity.Name.ToString();
            SqlConnection con = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter sda = new SqlDataAdapter();
            DataTable dt = new DataTable();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT Id,FolderName,ClientId" +
                               " FROM Folders";
            cmd.Connection = con;
            try
            {
                con.Open();
                foldersGrid.EmptyDataText = "No Records Found";
                sda.SelectCommand = cmd;
                sda.Fill(dt);
                foldersGrid.DataSource = dt;
                foldersGrid.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            this.BindData();
            foldersGrid.PageIndex = e.NewPageIndex;
            foldersGrid.DataBind();
        }

        protected void Edit(object sender, EventArgs e)
        {
            using (GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent)
            {
                ddlClients.Items.Clear();
                FillClientDropdown();
                txtFolderId.ReadOnly = true;
                tblFolderIdRow.Visible = true;
                txtFolderId.Text = row.Cells[0].Text;
                txtFolderName.Text = row.Cells[1].Text;
                hdnFolderName.Value = txtFolderName.Text;
                //ddlClients.SelectedIndex = ddlClients.Items.IndexOf(ddlClients.Items.FindByText(row.Cells[2].Text));
                string clientIds = row.Cells[2].Text;
                hdnClientId.Value = clientIds;
                List<string> stringList = clientIds.Split(',').ToList();
                foreach (string str in stringList)
                {
                    ddlClients.Items.FindByValue(str).Selected = true;
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), "myFunction", "myFunction();", true);
                pnlHeading.InnerText = "Update Folder";
                btnSave.Text = "Update";
                popup.Show();
            }
        }

        protected void Add(object sender, EventArgs e)
        {
            tblFolderIdRow.Visible = false;
            txtFolderId.Text = string.Empty;
            txtFolderName.Text = string.Empty;
            ddlClients.Items.Clear();
            FillClientDropdown();
            ScriptManager.RegisterStartupScript(this, typeof(Page), "myFunction", "myFunction();", true);
            pnlHeading.InnerText = "Add Folder";
            btnSave.Text = "Add";
            popup.Show();
            
        }

        protected void Save(object sender, EventArgs e)
        {
            string checkDuplicate = string.Empty;
            string clientIds = ClientIds();
            if (hdnFolderName.Value==txtFolderName.Text && hdnClientId.Value==clientIds)
            {
                checkDuplicate = "NotChanged";
            }
            else
            {
                checkDuplicate = "Changed";
            }
                int FolderId = 0;
            
                SqlConnection con = new SqlConnection(constr);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "AddUpdateFolder";
                cmd.Parameters.AddWithValue("@FolderId", txtFolderId.Text);
                cmd.Parameters.AddWithValue("@FolderName", txtFolderName.Text);
                cmd.Parameters.AddWithValue("@ClientId", clientIds);
                cmd.Parameters.AddWithValue("@Action", btnSave.Text.Trim());
                cmd.Parameters.AddWithValue("@CheckDuplicate", checkDuplicate);

            cmd.Connection = con;
                con.Open();
                FolderId = Convert.ToInt32(cmd.ExecuteScalar());
                con.Close();
                string message = string.Empty;
                switch (FolderId)
                {
                    case -1:
                        message = "FolderName already exists For this Client.\\nPlease choose a different FolderName.";
                        popup.Show();
                        break;
                    case -3:
                        message = "Updated Successfully";
                        BindData();
                        break;
                case -4:
                    message = "Updated Successfully";
                    BindData();
                    break;
                default:
                        message = "Inserted Successfully";
                    //string directoryPath = Server.MapPath("~/GoogleDocuments\\") + ddlClients.SelectedItem.Text+"\\"+txtFolderName.Text;
                    //if (!Directory.Exists(directoryPath))
                    //{
                    //    Directory.CreateDirectory(directoryPath);
                    //}
                    BindData();
                        break;
                }


                ScriptManager.RegisterStartupScript(this, typeof(Page), "Alert", "alert('" + message + "');", true);

            
        }

        private string ClientIds()
        {
            string clientIds = "";
            foreach (ListItem item in ddlClients.Items)
            {
                if (item.Selected)
                {
                    clientIds += item.Value+",";
                }
            }
            clientIds= clientIds.Remove(clientIds.Length - 1);
            return clientIds;
        }
        protected void DeleteFolder(object sender, EventArgs e)
        {
            //GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent;
            //string directoryPath = Server.MapPath("~/GoogleDocuments\\") + row.Cells[2].Text+"\\" + row.Cells[1].Text; 
            //if (Directory.Exists(directoryPath))
            //{
            //    Directory.Delete(directoryPath);
            //}
            LinkButton lnkRemove = (LinkButton)sender;
            SqlConnection con = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "DELETE FROM Folders WHERE " +
            "ID=@FolderId";
            cmd.Parameters.Add("@FolderId", SqlDbType.VarChar).Value
                = lnkRemove.CommandArgument;
            cmd.Connection = con;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            BindData();
        }

        protected void FillClientDropdown()
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT Id, Name FROM Clients"))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    ddlClients.DataSource = cmd.ExecuteReader();
                    ddlClients.DataTextField = "Name";
                    ddlClients.DataValueField = "Id";
                    ddlClients.DataBind();
                    con.Close();
                }
            }
            
        }
    }
}