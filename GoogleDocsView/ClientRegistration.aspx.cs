﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Security;
using BLL.GoogleDocsView;
using System.IO;

namespace GoogleDocsView
{
    public partial class ClientRegistration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }

        private void BindData()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = new BLLClientRegistration().BindClientDetails();

                clientGrid.DataSource = ds;
                clientGrid.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
  
        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            BindData();
            clientGrid.PageIndex = e.NewPageIndex;
            clientGrid.DataBind();
        }

        protected void Edit(object sender, EventArgs e)
        {
            using (GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent)
            {
                txtClientID.ReadOnly = true;
                tblCustomerIdRow.Visible = true;
                txtClientID.Text = row.Cells[0].Text;
                txtClientName.Text = row.Cells[1].Text;
                hdnClientName.Value = txtClientName.Text;
                pnlHeading.InnerText = "Update Client";
                btnSave.Text = "Update";
                popup.Show();
            }
        }

        protected void AddClient(object sender, EventArgs e)
        {
            tblCustomerIdRow.Visible = false;
            txtClientID.Text = string.Empty;
            txtClientName.Text = string.Empty;
            pnlHeading.InnerText = "Add Client";
            btnSave.Text = "Add";
            popup.Show();
        }

        protected void SaveClient(object sender, EventArgs e)
        {
            string checkDuplicate = string.Empty;
            if (hdnClientName.Value == txtClientName.Text.ToUpper())
            {
                checkDuplicate = "NotChanged";
            }
            else
            {
                checkDuplicate = "Changed";
            }

            int FolderId = new BLLClientRegistration().UpsertClientDetails(txtClientID.Text, txtClientName.Text.ToUpper(), btnSave.Text.Trim(), checkDuplicate);

            string message = string.Empty;
            switch (FolderId)
            {
                case -1:
                    message = "ClientName already exists.\\nPlease choose a different ClientName.";
                    popup.Show();
                    break;
                case -3:
                    message = "Updated Successfully";
                    string path = Server.MapPath("~/GoogleDocuments");
                    string Fromfol = "\\" + hdnClientName.Value + "\\";
                    string Tofol = "\\" + txtClientName.Text.ToUpper() + "\\";
                    Directory.Move(path + Fromfol, path + Tofol);
                    BindData();
                    break;
                case -4:
                    message = "Updated Successfully";
                    BindData();
                    break;
                default:
                    message = "Inserted Successfully";
                    string directoryPath = Server.MapPath("~/GoogleDocuments\\") + txtClientName.Text;
                    if (!Directory.Exists(directoryPath))
                    {
                        Directory.CreateDirectory(directoryPath);
                    }
                    BindData();
                    break;
            }

            ScriptManager.RegisterStartupScript(this, typeof(Page), "Alert", "alert('" + message + "');", true);
        }
 

        protected void DeleteClient(object sender, EventArgs e)
        {
            try
            {
                GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent;
                string directoryPath = Server.MapPath("~/GoogleDocuments\\") + row.Cells[1].Text;
                if (Directory.Exists(directoryPath))
                {
                    Directory.Delete(directoryPath);
                }
                LinkButton lnkRemove = (LinkButton)sender;
                string clientId = lnkRemove.CommandArgument;

                int effectedRows = new BLLClientRegistration().DeleteClient(clientId);

                BindData();
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}