﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.GoogleDocsView;

namespace GoogleDocsView
{
    public partial class FileDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }

            if (!this.IsPostBack)
            {
                //string value = HttpUtility.UrlEncode("https://www.iasted.org/conferences/formatting/Presentations-Tips.ppt");
                
                string userName    = this.User.Identity.Name.ToString();
                string roleName    = Session["RoleName"].ToString();
                string clientName  = Session["ClientName"].ToString();
                int clientId       = Convert.ToInt32(Session["ClientId"].ToString());
                int userId         = Convert.ToInt32(Session["UserId"].ToString());

                if (roleName == "SuperAdministrator")
                {
                    DirectoryInfo rootInfo = new DirectoryInfo(Server.MapPath("~/GoogleDocuments/"));
                    hplRegistration.Visible = true;
                    hplClient.Visible = true;
                    hplFolder.Visible = true;
                    this.PopulateTreeView(rootInfo, null);
                }
                else if (roleName == "Administrator")
                {
                    DirectoryInfo rootInfo = new DirectoryInfo(Server.MapPath("~/GoogleDocuments/"+clientName+"/"));
                    hplRegistration.Visible = true;

                    try
                    {
                        DataSet ds = new DataSet();
                        ds = new BLLFileDetails().GetALLFoldersByClient(clientId);

                        if (ds != null && ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                            {
                                this.PopulateTreeView(rootInfo, null, ds.Tables[0].Rows[i].ItemArray[1].ToString());
                            }
                        }
                    }
                  
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    try
                    {
                        DirectoryInfo rootInfo = new DirectoryInfo(Server.MapPath("~/GoogleDocuments/" + clientName + "/"));
                        string folderIds = GetFolderIds();

                        DataSet ds = new DataSet();
                        ds = new BLLFileDetails().GetAllFolderNameByFolderId(folderIds);
                        if (ds != null && ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                            {
                                this.PopulateTreeView(rootInfo, null, ds.Tables[0].Rows[i].ItemArray[0].ToString());
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    } 
                }
            }
        }

        public string GetFolderIds()
        {
            try
            {
                string adminUserIds = string.Empty;
                int userId = Convert.ToInt32(Session["UserId"].ToString());

                DataSet ds = new DataSet();
                ds = new BLLFileDetails().GetAllFolderIdsByUserId(userId);

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        string adminUserId = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                        adminUserIds += "'" + adminUserId + "'" + ",";

                    }
                }
                adminUserIds = adminUserIds.Remove(adminUserIds.Length - 1);
                return adminUserIds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void PopulateTreeView(DirectoryInfo dirInfo, TreeNode treeNode, string userName)
        {
            foreach (DirectoryInfo directory in dirInfo.GetDirectories())
            {
                if (directory.Name == userName)
                {
                    TreeNode directoryNode = new TreeNode
                    {
                        Text = directory.Name,
                        Value = directory.FullName
                    };

                    if (treeNode == null)
                    {
                        //If Root Node, add to TreeView.
                        TreeView1.Nodes.Add(directoryNode);
                    }
                    else
                    {
                        //If Child Node, add to Parent Node.
                        treeNode.ChildNodes.Add(directoryNode);
                    }

                    //Get all files in the Directory.
                    foreach (FileInfo file in directory.GetFiles())
                    {
                        //Add each file as Child Node.
                        TreeNode fileNode = new TreeNode
                        {
                            Text = file.Name,
                            
                            Target = "ContentRightFrame",
                            Value = HttpUtility.UrlEncode("https://docs.google.com/viewer?url=http://40.71.227.230:85/" + (new Uri(Server.MapPath("~/"))).MakeRelativeUri(new Uri(file.FullName)).ToString() + "&embedded=true"),
                            //string url = "http://www.google.com/search?q=" + HttpUtility.UrlEncode("Example");
                            //https://docs.google.com/viewer?url=https://www.iasted.org/conferences/formatting/Presentations-Tips.ppt&embedded=true
                            //NavigateUrl = "https://docs.google.com/viewer?url=http://52.170.215.104:85/" + (new Uri(Server.MapPath("~/"))).MakeRelativeUri(new Uri(file.FullName)).ToString() + "&embedded=true"

                        };
                        directoryNode.ChildNodes.Add(fileNode);
                    }

                    PopulateTreeView(directory, directoryNode, userName);

                }
            }
        }


        private void PopulateTreeView(DirectoryInfo dirInfo, TreeNode treeNode)
        {
            foreach (DirectoryInfo directory in dirInfo.GetDirectories())
            {

                TreeNode directoryNode = new TreeNode
                {
                    Text = directory.Name,
                    Value = directory.FullName
                };

                if (treeNode == null)
                {
                    //If Root Node, add to TreeView.
                    TreeView1.Nodes.Add(directoryNode);
                }
                else
                {
                    //If Child Node, add to Parent Node.
                    treeNode.ChildNodes.Add(directoryNode);
                }

                //Get all files in the Directory.
                foreach (FileInfo file in directory.GetFiles())
                {
                    //Add each file as Child Node.
                    TreeNode fileNode = new TreeNode
                    {
                        Text = file.Name,
                        Value = HttpUtility.UrlEncode("https://docs.google.com/viewer?url=http://40.71.227.230:85/" + (new Uri(Server.MapPath("~/"))).MakeRelativeUri(new Uri(file.FullName)).ToString() + "&embedded=true"),
                        //Value = "http://40.71.227.230:85/" + (new Uri(Server.MapPath("~/"))).MakeRelativeUri(new Uri(file.FullName)).ToString(),
                        Target = "ContentRightFrame",
                        //NavigateUrl = "https://docs.google.com/viewer?url=http://52.170.215.104:85/" + (new Uri(Server.MapPath("~/"))).MakeRelativeUri(new Uri(file.FullName)).ToString() + "&embedded=true"
                    
                    };
                    
                    directoryNode.ChildNodes.Add(fileNode);
                }

                PopulateTreeView(directory, directoryNode);



            }
        }

        protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
        {
            //string value = "https://view.officeapps.live.com/op/view.aspx?src="+ TreeView1.SelectedNode.Value;
            ContentRightFrame.Src = HttpUtility.UrlDecode(TreeView1.SelectedNode.Value);
        }
    }
}