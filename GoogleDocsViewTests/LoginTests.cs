﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BLL.GoogleDocsView;
using GoogleDocsView;

namespace GoogleDocsViewTests
{
    [TestClass]
    public class LoginTests
    {
        [TestMethod]
        public void CheckUsernamePasswordForEmpty_UsernamePasswordEmpty_ShouldReturnFalse()
        {
            //Arrang
            bool expected = false;

            //Act
            bool actual = new BLLLogin().CheckUsernamePasswordForEmpty("", "");

            //Assert
            Assert.AreEqual(expected, actual, "Method should return false when Username and Password are empty");

        }

        [TestMethod]
        public void CheckUsernamePasswordForEmpty_UsernameEmpty_ShouldReturnFalse()
        {
            //Arrang
            bool expected = false;

            //Act
            bool actual = new BLLLogin().CheckUsernamePasswordForEmpty("","Password");

            //Assert
            Assert.AreEqual(expected, actual,"Method should return false when UserName is empty");

        }

        [TestMethod]
        public void CheckUsernamePasswordForEmpty_PasswordEmpty_ShouldReturnFalse()
        {
            //Arrang
            bool expected = false;

            //Act
            bool actual = new BLLLogin().CheckUsernamePasswordForEmpty("Username", "");

            //Assert
            Assert.AreEqual(expected, actual, "Method should return false when Password is empty");

        }

        [TestMethod]
        public void CheckUsernamePasswordForEmpty_UsernamePasswordNotEmpty_ShouldReturnTrue()
        {
            //Arrang
            bool expected = true;

            //Act
            bool actual = new BLLLogin().CheckUsernamePasswordForEmpty("Username", "Password");

            //Assert
            Assert.AreEqual(expected, actual, "Method should return true when Username and Password are not empty");

        }

        [TestMethod]
        public void ValidateUser_InvalidUser_ShouldReturnNegetiveInteger()
        {
            //Arrang
            int expected = -1;

            //Act
            UserDetails userDetails = new BLLLogin().ValidateUser("swarupkumar123", "12345");
            int actual = userDetails.UserId;

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ValidateUser_InvalidPassword_ShouldReturnNegetiveInteger()
        {
            //Arrang
            int expected = -1;

            //Act
            UserDetails userDetails = new BLLLogin().ValidateUser("swarupkumar", "121212");
            int actual = userDetails.UserId;

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ValidateUser_UserNotActive_ShouldReturnNegetiveValue()
        {
            //Arrang
            int expected = -2;

            //Act
            UserDetails userDetails = new BLLLogin().ValidateUser("HrisshikheshReddy", "12345");
            int actual = userDetails.UserId;

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ValidateUser_ValidUsernamePassword_ShouldReturnPositiveInteger()
        {
            //Arrang
            int expected = 23;

            //Act
            UserDetails userDetails = new BLLLogin().ValidateUser("swarupkumar", "12345");
            int actual = userDetails.UserId;

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
